/*** EJERICIO 1 */
function estiljs() {
    let p = document.getElementById("text");
    p.style.color = "red";
    p.style.backgroundColor = "yellow";
    p.style.textTransform = "uppercase";
}


/*** EJERICIO 2 */
function prenValorForm() {
    let input = document.getElementsByTagName("input");
    let text = "nom " + input[0].value + " apellido " + input[1].value;
    console.log(text);

}

/*** EJERICIO 3 */
function ejercicio3() {
    let article = document.getElementById("art3");
    let p = article.getElementsByTagName("p");
    p[0].style.color = "red";
    p[1].style.backgroundColor = "yellow";
    p[2].style.textTransform = "uppercase";
    p[3].style.textDecoration = "underline";
}

/*** EJERICIO 4 */
function obtenirAtributs() {
    let article = document.getElementById("art4");
    let a = article.getElementsByTagName("a");
    console.log(a[0].attributes);

    //opcion2
    let nodeMap = a[0].attributes;
    let text = "*Listado: ";
    for (let i = 0; i < nodeMap.length; i++) {
        text += nodeMap[i].name + " = " + nodeMap[i].value;
    }

    console.log(text);
}

/*** EJERICIO 5 */
function insertarFila() {
    let tabla = document.getElementById("Taula");
    let fila = document.createElement("tr");
    for (let i = 1; i <= 2; i++) {
        let celda = document.createElement("td");
        celda.innerText = "Fila 3 celda " + i;
        fila.appendChild(celda);
    }
    tabla.insertBefore(fila, tabla.firstChild);
}

/*** EJERICIO 7 */
function crearTabla() {
    let tabla = document.getElementById("tabla");

    let input = document.getElementsByTagName("input");
    let f = input["fila"].value;
    let c = input["columna"].value;

    for (let i = 0; i < f; i++) {
        let fila = document.createElement("tr");
        for (let j = 1; j <= c; j++) {
            let celda = document.createElement("td");
            celda.innerText = "Fila" + i + "Celda " + j;
            fila.appendChild(celda);
        }
        tabla.insertBefore(fila, tabla.firstChild);
    }
}

/**** EJERCCIO 8 */
function borrar(){
    let valor = document.getElementById("dropDownList").value;
    for(let option of document.getElementById("dropDownList").options){
        console.log(option);
        if (option.value == valor){
            option.remove();
        }
    }
}

/**** EJERCCIO 9 */
function añade(){
    let numLista = prompt("¿Cuantos elementos quieres?")
    let dropList = document.getElementById("dropDownList");
    for(let i = 0;i<numLista;i++){
        option=document.createElement("option");
        option.value = prompt("Valor del elemento");
        option.innerText = option.value;
        dropList.appendChild(option);
    }
}

/**** EJERCCIO 10 */
function volume_sphere()
{
    var volume;
    var radius = document.getElementById('radius').value;
    radius = Math.abs(radius);
    volume = (4/3) * Math.PI * Math.pow(radius, 3);
    volume = volume.toFixed(4);
    document.getElementById('volume').value = volume;
    return false;
}



//Añado la función onClick del boton
window.onload = document.getElementById("form10").onsubmit = volume_sphere;

/**** EJERCCIO 11 */
    //alamcenamos la lista de elementos STRONG
var strong_Items;
window.onload = getStrong_items();

function getStrong_items()
{
    strong_Items = document.getElementsByTagName('strong');
}


function resaltarStrong()
{
    for (var i=0; i<strong_Items.length; i++)
    {
        strong_Items[i].style.color = "red";
    }
}


function normalizarStrong()
{
    for (var i=0; i<strong_Items.length; i++)
    {
        strong_Items[i].style.color = "black";
    }
}