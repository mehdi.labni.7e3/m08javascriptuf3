/*
Autor : Mehdi Labni
Curs : 1DAWe
*/

const codigo = [];
const maxIntento = 8;
let intento = 0;

let arrayValores = document.getElementById("numero");
let info = document.getElementById("info")
let infoClass = document.getElementById("infoClass")


/*1. Genera una constante CODIGO_SECRETO de tipo array de 5 número aleatorios entre 0 y 9 usando la libreria Math.random();*/
function codigoSecreto() {
    for (let i = 0; i < 5; i++) {
        codigo[i] = Math.floor((Math.random() * 10));
    }
}

codigoSecreto();

function crearFilaCasillas() {
    let tabla = document.getElementById("Result");
    for (let i = 0; i < maxIntento; i++) {
        let fila = document.createElement("div");
        fila.className = "rowResult w100 flex wrap";
        tabla.appendChild(fila);
        for (let i = 0; i < 5; i++) {
            let casilla = document.createElement("div");
            casilla.className = "w20";
            let celda = document.createElement("div");
            celda.className = "celResult flex";
            casilla.appendChild(celda);
            fila.appendChild(casilla);

        }
    }
}
crearFilaCasillas();

function insertarValor() {
    let fila = document.getElementsByClassName("rowResult w100 flex wrap");
    let casilla = fila[intento].getElementsByClassName("celResult flex");
    let intentosRestantes = maxIntento-intento

    if (intento >= maxIntento-3 ){
        info.innerText = "Te quedan "+ intentosRestantes + ". ALERTA VA VA!!!"
        infoClass.style.backgroundColor= "coral"
        infoClass.style.borderColor= "coral"
    }else {
        info.innerText = "Te quedan "+ intentosRestantes + ". VAMOS!!!"
    }

    for (let i = 0; i < 5; i++) {
        casilla[i].innerHTML = arrayValores.value[i]
        if (casilla[i].innerText == codigo[i]) {
            casilla[i].style.backgroundColor = "green"
        } else if (codigo.includes(parseInt(casilla[i].innerHTML))) {
            casilla[i].style.backgroundColor = "orangered"
        }

    }
}

function mostarResultat() {
    let casilla = document.getElementsByClassName("cel flex");
    console.log(casilla)
    for (let i = 0; i < 5; i++) {
        casilla[i].innerHTML = codigo[i]
        console.log(codigo[i])
    }
}

function comparar() {
    let arrayValores = document.getElementById("numero").value;

    if (arrayValores === codigo.join("")) {
        return true
    }
}


console.log(codigo.join(""))


function Comprobar() {
    let button = document.getElementById("check")

    if (button.textContent == "Reiniciar") {
        location.reload()
    }
    insertarValor();
    intento++;
    if (comparar()) {
        mostarResultat()
        button.innerHTML = "Reiniciar"
        info.innerText = "Felicidades has ganado"
    } else if (maxIntento === intento) {
        info.style.color = "white"
        info.innerText = "Has llegado al maximo de intentos"
        infoClass.style.backgroundColor = "red"
        infoClass.style.borderColor = "red"
        mostarResultat()
        button.innerHTML = "Reiniciar"
    }

}
